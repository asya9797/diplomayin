import {Component,OnInit, ViewChild} from '@angular/core';
import { SwiperModule, SwiperDirective } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
 

 

@Component({
    selector: "header",
    templateUrl: "./header.component.html",
    styleUrls : ["./header.component.scss"]

})

export class HeaderComponent implements OnInit{
   
      public config: SwiperConfigInterface = {
        direction: 'vertical',
        slidesPerView: 1,
        keyboard: true,
        mousewheel: true,
        pagination:true,

      };
    
    
    
      @ViewChild(SwiperDirective) directiveRef: SwiperDirective;
    
      constructor() {}
    
      onClick() {
        console.log (this.directiveRef.swiper().activeIndex);
      }
      public onIndexChange(index: number) {
        console.log('Swiper index: ', index);
      }
      ngOnInit(){}
}

