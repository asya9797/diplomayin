import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { StatisticsService } from "./statistics.service";
import { StatisticsRoutingModule } from "./statistics-routing.module";
import { StatisticsComponent } from "./statistics.component";
import { SharedModule } from "../../shared/shared.module";
import { ServerRequests } from "../../services/serverRequests.service";




@NgModule({
    declarations:[StatisticsComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,StatisticsRoutingModule,SharedModule],
    exports:[],
    providers:[StatisticsService,ServerRequests],
    entryComponents: []
})
export class StatisticsModule {}