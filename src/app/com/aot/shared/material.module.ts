import { NgModule } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule, MatDialogModule, MatNativeDateModule, MatRadioModule} from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTableModule} from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatDatepickerModule } from "@angular/material";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatIconModule} from '@angular/material/icon';
import {MatTabsModule} from '@angular/material/tabs';

@NgModule({
    declarations: [],
    imports: [MatTabsModule,MatRadioModule,MatIconModule,MatNativeDateModule,MatDatepickerModule,MatFormFieldModule,MatInputModule,MatDialogModule,MatSelectModule,MatTableModule,MatExpansionModule,MatProgressSpinnerModule,MatCheckboxModule,MatSlideToggleModule],
    exports: [MatTabsModule,MatRadioModule,MatIconModule,MatNativeDateModule,MatDatepickerModule,MatFormFieldModule,MatInputModule,MatDialogModule,MatSelectModule,MatTableModule,MatExpansionModule,MatProgressSpinnerModule,MatCheckboxModule,MatSlideToggleModule]
})
export class MaterialModule { }