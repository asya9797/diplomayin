import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
const reviews = [
    {
        text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been  the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley",
        time: "13:12",
        date: "14.03.2019",

    },
    {
        text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Contrary to popular belief, Lorem Ipsum is not simply random ",
        time: "15:12",
        date: "15.03.2019",

    },
    {
        text: "Lorem Ipsum is simply dummy text of the printing",
        time: "16:12",
        date: "17.03.2019",

    },
]
@Component({
    selector: 'review',
    templateUrl: './review.component.html',
    styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
    public reviews = reviews;
    public topBarTitle:string = "REVIEW"
    constructor() { }
    ngOnInit() { 
       
    }


  
}