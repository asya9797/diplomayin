import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { UserInfoComponent } from "./user-info.component";
const routesUserInfo: Routes = [
    {
        path:"",
        component: UserInfoComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesUserInfo)],
    exports:[RouterModule]
})
export class UserInfoRoutingModule{}