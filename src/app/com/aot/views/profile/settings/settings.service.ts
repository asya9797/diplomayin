import { Injectable } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class SettingsService {
    constructor(private _apiService: ApiService,private _httpClient: HttpClient) { }

    public  getCountriesList(){
        return this._httpClient.get("/assets/countries.json")
    }
    public getMyData():Observable<object | any>{
        return this._apiService.get('client/me')
     }
}