import { Injectable } from '@angular/core';
import { ApiService } from '../../../services/api.service';
import { Observable } from 'rxjs';

@Injectable()
export class UserInfoService {
    constructor(private _apiService: ApiService) { }

    public getMyData():Observable<object | any>{
        return this._apiService.get('client/me')
     }
}