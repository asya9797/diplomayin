import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PageOfCityService } from './page-of-city.service';
import { ActivatedRoute } from '@angular/router';

declare var google: any;
@Component({
    selector: 'page-of-city',
    templateUrl: './page-of-city.component.html',
    styleUrls: ['./page-of-city.component.scss']
})
export class PageOfCityComponent implements OnInit {
    public map;
    private _marker;
    public idOfCity:number;
    public cityDatas;
    constructor(private _pageOfCityService: PageOfCityService,private _activatedRoute: ActivatedRoute) { 
        this._activatedRoute.queryParams
        .subscribe(params => {
            if (params) {
                console.log(params.cityId);
                this.idOfCity = params.cityId
            }
        });
    }
    ngOnInit() {
        this.getCityDatas();
       
     
    }


    private _initMap(corrdinates = { lat: this.cityDatas.latitude, lng: this.cityDatas.longitude }, zoom = 15): void {
        var uluru = corrdinates
        this.map = new google.maps.Map(document.getElementById('map'), {
            zoom: zoom,
            center: uluru,
            zoomControl: true,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: true,
            rotateControl: false,
            fullscreenControl: false
        });
        this._marker = new google.maps.Marker({
            position: corrdinates,
            map: this.map,

        });
      
    }
    public getCityDatas(){
        this._pageOfCityService.getCityById(this.idOfCity).subscribe((data) => {
            console.log(data,"cityi getyy");
            this.cityDatas = data;
            this._initMap()
            
        })
    }


}