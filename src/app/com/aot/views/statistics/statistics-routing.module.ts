import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { StatisticsComponent } from "./statistics.component";
const routesStatistics: Routes = [
    {
        path:"",
        component: StatisticsComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesStatistics)],
    exports:[RouterModule]
})
export class StatisticsRoutingModule{}