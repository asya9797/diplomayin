import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class CountriesService {
    constructor(private _httpClient:HttpClient) { }
    
    public  getCountriesList(){
        return this._httpClient.get("/assets/countries.json")
    }
}