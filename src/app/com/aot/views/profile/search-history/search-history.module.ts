import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SearchHistoryService } from "./search-history.service";
import { SearchHistoryRoutingModule } from "./search-history-routing.module";
import { SearchHistoryComponent } from "./search-history.component";
import { SharedModule } from "../../../shared/shared.module";



@NgModule({
    declarations:[SearchHistoryComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,SearchHistoryRoutingModule,SharedModule],
    exports:[],
    providers:[SearchHistoryService]
})
export class SearchHistoryModule {

}