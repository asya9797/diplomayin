import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { LoginService } from "./login.service";
import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./login.component";
import { SharedModule } from "../../shared/shared.module";
import { ServerRequests } from "../../services/serverRequests.service";
import { CookieService } from "angular2-cookie/core";


@NgModule({
    declarations:[LoginComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,LoginRoutingModule,SharedModule],
    exports:[],
    providers:[LoginService,ServerRequests,CookieService]
})
export class LoginModule {

}