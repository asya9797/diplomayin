import {Component,OnInit} from '@angular/core';

@Component({
    selector: "main-header",
    templateUrl: "./main-header.component.html",
    styleUrls : ["./main-header.component.scss"]

})

export class MainHeaderComponent implements OnInit{
    public menuForServices:boolean = false;

    constructor(){}
    ngOnInit(){}

    public openMenu(){
        this.menuForServices = !this.menuForServices
    }
    public onClickedOutside() {
     
        setTimeout(()=>{
          if (this.menuForServices) {
           
            this.menuForServices = false
          }
        })
       
      }
}
