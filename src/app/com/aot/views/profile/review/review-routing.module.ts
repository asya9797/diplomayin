import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { ReviewComponent } from "./review.component";
const routesReview: Routes = [
    {
        path:"",
        component: ReviewComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesReview)],
    exports:[RouterModule]
})
export class ReviewRoutingModule{}