import {Pipe,PipeTransform} from '@angular/core'

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {
    transform(val: string) {
        if (val.length > 50) {
            return val.substr(0, 50) + '...'
        } else {
            return val
        }
    }
}
