import { Component, OnInit } from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';
import { Router } from '@angular/router';

const statesImgs: Array<any> = [

    {
        img: '/assets/assets/images/nkar1.jpg',
    },
    {
        img: '/assets/assets/images/nkar2.jpg',
    },
    {
        img: '/assets/assets/images/nkar3.jpg',
    },


    {
        img: '/assets/assets/images/nkar4.jpg',
    },
    {
        img: '/assets/assets/images/nkar5.jpg',
    },
    {
        img: '/assets/assets/images/nkar6.jpg',
    },


    {
        img: '/assets/assets/images/nkar7.jpg',
    },
    {
        img: '/assets/assets/images/nkar8.jpg',
    },
    {
        img: '/assets/assets/images/nkar9.jpg',
    }

]
@Component({
    selector: "countries-advertisement",
    templateUrl: "./countries-advertisement.component.html",
    styleUrls: ["./countries-advertisement.component.scss"]

})

export class CountriesAdvertisementComponent implements OnInit {
   
    public carouselTileItems: Array<any> = [];
    public carouselTile: NgxCarousel;
    constructor(private _router: Router) { }
    ngOnInit() {
        this.carouselTile = {
            grid: { xs: 2, sm: 3, md: 3, lg: 4, all: 0 },
            speed: 500,
            interval: 3000,
            point: {
       
                visible: false,
                pointStyles: `
                .ngxcarouselPoint {
                  list-style-type: none;
                  text-align: center;
                  padding: 12px;
                  margin: 0;
                  white-space: nowrap;
                  overflow: auto;
                  box-sizing: border-box;
                }
                .ngxcarouselPoint li {
                  display: inline-block;
                  border-radius: 50%;
                  border: 2px solid rgba(0, 0, 0, 0.55);
                  padding: 4px;
                  margin: 0 3px;
                  transition-timing-function: cubic-bezier(.17, .67, .83, .67);
                  transition: .4s;
                }
                .ngxcarouselPoint li.active {
                    background: #6b6b6b;
                    transform: scale(1.2);
                }
              `
            },
            load: 3,
            touch: true
        };
        this.carouselTileLoad();
    }

    public carouselTileLoad() {
      this.carouselTileItems = statesImgs
    }

    public goToCountriesPage(){
        this._router.navigate(['/countries'])
    }
}
