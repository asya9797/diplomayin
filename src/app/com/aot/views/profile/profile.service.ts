import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../../services/api.service';

@Injectable()
export class ProfileService {

    constructor(private _apiService: ApiService) { }

    public getMyData():Observable<object | any>{
        return this._apiService.get('client/me')
     }
}