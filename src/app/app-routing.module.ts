import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './com/aot/services/authguard.service';
const routes: Routes = [
  {
    path: '',
    loadChildren: 'src/app/com/aot/views/main/main.module#MainModule'
  },
  {
    path: "login",
    loadChildren: "src/app/com/aot/views/login/login.module#LoginModule"
  },
  {
    path: "registration",
    loadChildren: "src/app/com/aot/views/registration/registration.module#RegistrationModule"
  },
  {
    path: "forgot",
    loadChildren: "src/app/com/aot/views/forgot/forgot.module#ForgotModule"
  },
  {
    path: "tickets",
    loadChildren: "src/app/com/aot/views/statistics/statistics.module#StatisticsModule"
  },
  {
    path: "profile",
    loadChildren: "src/app/com/aot/views/profile/profile.module#ProfileModule",
    // canActivate: [AuthGuard]
  },
  {
    path: "countries",
    loadChildren: "src/app/com/aot/views/countries/countries.module#CountriesModule"
  },
  {
    path: "cities",
    loadChildren: "src/app/com/aot/views/cities/cities.module#CitiesModule"
  },
  {
    path: "cities/:id",
    loadChildren: "src/app/com/aot/views/page-of-city/page-of-city.module#PageOfCityModule"
  }
  
];

@NgModule({
  imports:[RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }