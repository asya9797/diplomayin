export interface CountriesList {
    name: string;
    code: string
}
export interface ClientDatasForRegistration {
    username: string;
    email: string;
    address: string;
    phoneNum: string;
    gender: string;
    age: number;
    city: string;
    country: string;
    zipCode: number;
    password: string;
}
export interface AccessToken {
    accessToken: string
}
export interface LoginFields {
    email: string;
    password: string
}
export interface ClientDatas {
    userName: string;
    address: string;
    age: number;
    city: string;
    country: string;
    email: string;
    gender: string;
    histories: Array<any>
    id: number
    password: string;
    phone: string;
    zipCode: number
}
export interface DataOfClientForShow{
    name: string;
    value: string
}