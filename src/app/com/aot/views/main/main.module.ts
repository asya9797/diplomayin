import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MainService } from "./main.service";
import { MainRoutingModule } from "./main-routing.module";
import { MainComponent } from "./main.component";
import { SharedModule } from "../../shared/shared.module";
import { MatDatepickerModule } from "@angular/material";
import { MaterialModule } from "../../shared/material.module";
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { ServerRequests } from "../../services/serverRequests.service";

@NgModule({
    declarations: [MainComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule, MainRoutingModule,SharedModule,MaterialModule],
    exports: [],
    providers: [MainService,ServerRequests]
})
export class MainModule {

}