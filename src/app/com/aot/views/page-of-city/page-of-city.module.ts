import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PageOfCityService } from "./page-of-city.service";
import { PageOfCityRoutingModule } from "./page-of-city-routing.module";
import { PageOfCityComponent } from "./page-of-city.component";
import { SharedModule } from "../../shared/shared.module";
import { ServerRequests } from "../../services/serverRequests.service";



@NgModule({
    declarations:[PageOfCityComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,PageOfCityRoutingModule,SharedModule],
    exports:[],
    providers:[PageOfCityService,ServerRequests],
    entryComponents: []
})
export class PageOfCityModule {}