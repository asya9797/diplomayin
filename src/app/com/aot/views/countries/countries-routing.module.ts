import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { CountriesComponent } from "./countries.component";
const routesCountries: Routes = [
    {
        path:"",
        component: CountriesComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesCountries)],
    exports:[RouterModule]
})
export class CountriesRoutingModule{}