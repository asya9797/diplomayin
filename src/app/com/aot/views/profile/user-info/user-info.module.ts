import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserInfoService } from "./user-info.service";
import { UserInfoRoutingModule } from "./user-info-routing.module";
import { UserInfoComponent } from "./user-info.component";
import { SharedModule } from "../../../shared/shared.module";



@NgModule({
    declarations:[UserInfoComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,UserInfoRoutingModule,SharedModule],
    exports:[],
    providers:[UserInfoService]
})
export class UserInfoModule {

}