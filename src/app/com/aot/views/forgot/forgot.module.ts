import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ForgotService } from "./forgot.service";
import { ForgotRoutingModule } from "./forgot-routing.module";
import { ForgotComponent } from "./forgot.component";


@NgModule({
    declarations:[ForgotComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,ForgotRoutingModule],
    exports:[],
    providers:[ForgotService],
    entryComponents: []
    
})
export class ForgotModule {

}