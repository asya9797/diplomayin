import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserInfoService } from './user-info.service';
import { ClientDatas, DataOfClientForShow } from '../../../models/models';

@Component({
    selector: 'user-info',
    templateUrl: './user-info.component.html',
    styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
    public topBarTitle: string = "INFO";
    public datas;
    constructor(private _userInfoService: UserInfoService) { }
    ngOnInit() {
       this.getMyData()
    }

    public getMyData(): void {
        this._userInfoService.getMyData().subscribe((data: ClientDatas) => {
            console.log(data, "my data");
            this.datas = [
                {
                    name: "USERNAME",
                    value: data.userName
                },
                {
                    name: "EMAIL",
                    value: data.email
                },
                {
                    name: "PHONE",
                    value: data.phone
                },
                {
                    name: "ADDRESS",
                    value: data.address
                },
                {
                    name: "AGE",
                    value: data.age
                },
                {
                    name: "GENDER",
                    value: data.gender
                },
                {
                    name: "COUNTRY",
                    value: data.country
                },
                {
                    name: "CITY",
                    value: data.city
                },
                {
                    name: "ZIP CODE",
                    value: data.zipCode
                },
            
            ]
        })


    }
}