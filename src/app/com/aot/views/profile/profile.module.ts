import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ProfileService } from "./profile.service";
import { ProfileRoutingModule } from "./profile-routing.module";
import { ProfileComponent } from "./profile.component";
import { SharedModule } from "../../shared/shared.module";


@NgModule({
    declarations:[ProfileComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,ProfileRoutingModule,SharedModule],
    exports:[],
    providers:[ProfileService]
})
export class ProfileModule {

}