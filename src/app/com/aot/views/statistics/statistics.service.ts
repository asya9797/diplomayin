import { Injectable } from '@angular/core';
import { ServerRequests } from '../../services/serverRequests.service';



@Injectable()
export class StatisticsService {
    constructor(private _serverRequestsService:ServerRequests) { }
    
    public getTickets(origin,destination,day){
        return this._serverRequestsService.get(`search/basic?origin=${origin}&destination=${destination}&trip_duration=1&beginning_of_period=${day}&sorting=price&currency=rub`)
    }
}