import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServerRequests } from '../../services/serverRequests.service';

@Injectable()
export class RegistrationService{
    constructor(private _httpClient: HttpClient,private _serverRequestsService: ServerRequests) { }


    public  getCountriesList(){
        return this._httpClient.get("/assets/countries.json")
    }

    public postClientDatasForRegistration(body:object):Observable<object | any>{
        return this._serverRequestsService.post('client/register',body)
     }
}