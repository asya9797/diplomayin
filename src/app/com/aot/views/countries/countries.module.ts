import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CountriesService } from "./countries.service";
import { CountriesRoutingModule } from "./countries-routing.module";
import { CountriesComponent } from "./countries.component";
import { SharedModule } from "../../shared/shared.module";



@NgModule({
    declarations:[CountriesComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,CountriesRoutingModule,SharedModule],
    exports:[],
    providers:[CountriesService],
    entryComponents: []
})
export class CountriesModule {}