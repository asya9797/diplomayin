import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CitiesService } from "./cities.service";
import { CitiesRoutingModule } from "./cities-routing.module";
import { CitiesComponent } from "./cities.component";
import { ServerRequests } from "../../services/serverRequests.service";
import { SharedModule } from "../../shared/shared.module";


@NgModule({
    declarations:[CitiesComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,CitiesRoutingModule,SharedModule],
    exports:[],
    providers:[CitiesService,ServerRequests],
    entryComponents: []
    
})
export class CitiesModule {

}