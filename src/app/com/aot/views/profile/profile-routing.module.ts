import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { ProfileComponent } from "./profile.component";
const routesProfile: Routes = [
    {
        path:"",
        component: ProfileComponent,
        children: [
            {
                path: 'userInfo',
                loadChildren: "src/app/com/aot/views/profile/user-info/user-info.module#UserInfoModule"
            },
            {
                path: '',
                redirectTo:'userInfo',
                pathMatch:'full'
            },
            {
                path: 'settings',
                loadChildren: "src/app/com/aot/views/profile/settings/settings.module#SettingsModule"
            },
            {
                path: 'historyOfSearch',
                loadChildren: "src/app/com/aot/views/profile/search-history/search-history.module#SearchHistoryModule"
            },
            {
                path: 'review',
                loadChildren: "src/app/com/aot/views/profile/review/review.module#ReviewModule"
            },
        ]
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesProfile)],
    exports:[RouterModule]
})
export class ProfileRoutingModule{}