import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { CitiesComponent } from "./cities.component";
const routesCities: Routes = [
    {
        path:"",
        component: CitiesComponent,
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesCities)],
    exports:[RouterModule]
})
export class CitiesRoutingModule{}