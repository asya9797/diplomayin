import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SettingsService } from './settings.service';
import { subscribeOn } from '../../../../../../../node_modules/rxjs/operators';
import { CountriesList } from '../../../models/models';
const  datas = [
    {
        name: "USERNAME",
        controlname: "username",
    },
    {
        name: "EMAIL",
        controlname: "email",
    },
    {
        name: "PHONE",
        controlname: "phone"
    },
    {
        name: "ADDRESS",
        controlname: "address"
    },
    {
        name: "AGE",
        controlname: "age"
    },
    {
        name: "GENDER",
        controlname: "gender"
    },
    {
        name: "COUNTRY",
        controlname: "country"
    },
    {
        name: "CITY",
        controlname: "city"
    },
    {
        name: "ZIP CODE",
        controlname: "zipCode"
    },

]
@Component({
    selector: 'settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
    public topBarTitle:string = "SETTINGS";
    public settingsForm: FormGroup;
    public countries:Array<CountriesList>;
    public datas = datas;
    constructor(private _settingsService: SettingsService) { }
    ngOnInit() { 
       this._buildForm();
       this.getCountriesList()
       this.getMyData()
    }
    private _buildForm() {
        this.settingsForm = new FormBuilder().group({
            username: ['', Validators.required],
            email: ['', Validators.required], 
            address: ['', Validators.required],
            phone: ['', Validators.required],
            age: ['', Validators.required],
            city: ['', Validators.required],
            country: ['', Validators.required],
            zipCode: ['', Validators.required],
            // password: ['', Validators.required],
            gender: ['', Validators.required]
        })
    }

    public getMyData(){
        this._settingsService.getMyData().subscribe((data:ClientData) => {
            this.seeOldInfoOfUser(data)
        })
    }
    public seeOldInfoOfUser(data): void {
        if (data) {
            console.log(data);
            this.settingsForm.patchValue({
                username: data.userName,
                email: data.email,
                address: data.address,
                phone: data.phone,
                age: data.age,
                country :  data.country,
                city :  data.city,
                zipCode :  data.zipCode,
                // password :  data.city,
                gender :  data.gender
            })

        }
    }

    public getCountriesList() {
        this._settingsService.getCountriesList().subscribe((data:Array<CountriesList>) => {
            console.log(data);
            this.countries = data

        })
    }


  
}