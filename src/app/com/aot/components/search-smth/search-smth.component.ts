import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Host, ChangeDetectorRef, Inject } from '@angular/core';
import { MatCalendar } from '@angular/material';
import { DateAdapter, MAT_DATE_FORMATS, MatDateFormats } from '@angular/material/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: "search-smth",
  templateUrl: "./search-smth.component.html",
  styleUrls: ["./search-smth.component.scss"],


})

export class SearchSmthComponent implements OnInit {
  public selected;
  public ticketType: string = "Economy";
  public sortingCards: boolean = false;
  public exampleHeader = ExampleHeader;
  public statuses = status;
  public countsList: boolean = false;
  public generalCount: number = 1;
  public adultsCount: number = 1;
  public childrenCount: number = 0;
  public babiesCount: number = 0;
  public options = {
    types: [],
    componentRestrictions: { country: 'UA' }
  }
  constructor(private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {

  }
  public popUpForSortCards(): void {
    this.sortingCards = !this.sortingCards
    console.log(this.sortingCards)
  }

  public openPassangersCountList(): void {
    this.countsList = !this.countsList
    console.log(this.countsList);

  }
  public handleAddressChange(address) {
    console.log(address);

  }
  public ascendingAdult() {
    this.adultsCount++;
    this.generalCount++
  }
  public descendingAdult() {
    if (this.adultsCount > 1) {
      this.adultsCount--;
      this.generalCount--
    } else {
      this.adultsCount - 0
    }

  }

  public ascendingChildren() {
    this.childrenCount++;
    this.generalCount++
  }
  public descendingChildren() {
    if (this.childrenCount > 0) {
      this.childrenCount--;
      this.generalCount--
    } else {
      this.childrenCount - 0
    }
  }

  public ascendingBabies() {
    this.babiesCount++;
    this.generalCount++
  }
  public descendingBabies() {
    if (this.babiesCount > 0) {
      this.babiesCount--;
      this.generalCount--
    } else {
      this.babiesCount - 0
    }
  }
  public onClickedOutside() {
    // this.countsList =! this.countsList
    setTimeout(() => {
      if (this.countsList) {
        console.log("bareev");
        this.countsList = false
      }
    })

  }

  public onClickSearch(): void {
    this._router.navigate(['/tickets'], { relativeTo: this._activatedRoute, queryParams: {"origin":"AM","destination":"RU","day": "2019-05-21"} })
  }

}

@Component({
  selector: 'example-header',
  styles: [`
      .example-header {
        display: flex;
        align-items: center;
        padding: 0.5em;
        height: 40px;
        font-size: 18px;
        font-family: Lato Bold;
        color: black;
      }
  
      .example-header-label {
          flex: 1;
          height: 24px;
          margin: auto;
          text-align: center;
         
      }
  
      .example-double-arrow .mat-icon {
        margin: -22%;
        
      }
      .mat-icon-button{
        outline: 0px dotted;
      }
     
    `],
  template: `
      <div class="example-header">
    
        <button mat-icon-button (click)="previousClicked('month')">
          <mat-icon style=" color: #a5a6a9;">keyboard_arrow_left</mat-icon>
        </button>
        <span class="example-header-label">{{periodLabel}}</span>
        <button mat-icon-button (click)="nextClicked('month')">
          <mat-icon style=" color: #a5a6a9;">keyboard_arrow_right</mat-icon>
        </button>
       
      </div>
    `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExampleHeader<D> implements OnDestroy {
  private destroyed = new Subject<void>();


  constructor(@Host() private calendar: MatCalendar<D>,
    private dateAdapter: DateAdapter<D>,
    @Inject(MAT_DATE_FORMATS) private dateFormats: MatDateFormats,
    cdr: ChangeDetectorRef) {
    calendar.stateChanges
      .pipe(takeUntil(this.destroyed))
      .subscribe(() => cdr.markForCheck());
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
  }

  get periodLabel() {
    return this.dateAdapter
      .format(this.calendar.activeDate, this.dateFormats.display.monthYearLabel)
      .toLocaleUpperCase();
  }

  previousClicked(mode: 'month' | 'year') {
    this.calendar.activeDate = mode === 'month' ?
      this.dateAdapter.addCalendarMonths(this.calendar.activeDate, -1) :
      this.dateAdapter.addCalendarYears(this.calendar.activeDate, -1);
  }

  nextClicked(mode: 'month' | 'year') {
    this.calendar.activeDate = mode === 'month' ?
      this.dateAdapter.addCalendarMonths(this.calendar.activeDate, 1) :
      this.dateAdapter.addCalendarYears(this.calendar.activeDate, 1);
  }




}