import { NgModule,  } from '@angular/core';
import { HeaderComponent} from '../components/header/header.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MainHeaderComponent } from '../components/main-header/main-header.component';
import { SearchSmthComponent, ExampleHeader } from '../components/search-smth/search-smth.component';
import { CountriesAdvertisementComponent } from '../components/countries-advertisement/countries-advertisement.component';
import { NgxCarouselModule } from 'ngx-carousel';
import { AboutUsComponent } from '../components/about-us/about-us.component';
import { FooterComponent } from '../components/footer/footer.component';
import { MaterialModule } from './material.module';
import { ProfileItemTopPlaceComponent } from '../components/profile-item-top-place/profile-item-top-place.component';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDaterangepickerMd, LocaleService } from 'ngx-daterangepicker-material';
import { OnlyNumber } from '../directives/onlyNumber.directive';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { ClickOutsideModule } from 'ng-click-outside'; 
import { TruncatePipe } from '../services/truncate.pipe';
 
const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  observer: true,
  direction: 'vertical',
  slidesPerView: 1,
  centeredSlides: true,
  };
@NgModule({
    declarations: [TruncatePipe,OnlyNumber,ProfileItemTopPlaceComponent,HeaderComponent,MainHeaderComponent,SearchSmthComponent,ExampleHeader,CountriesAdvertisementComponent,AboutUsComponent,FooterComponent],
    imports: [ClickOutsideModule,GooglePlaceModule,SwiperModule,CommonModule,RouterModule,ReactiveFormsModule,FormsModule,NgxCarouselModule,MaterialModule,FlexLayoutModule,NgxDaterangepickerMd.forRoot()],
    exports: [TruncatePipe,ClickOutsideModule,GooglePlaceModule,OnlyNumber,SwiperModule,ProfileItemTopPlaceComponent,FooterComponent,AboutUsComponent,HeaderComponent,CommonModule,FormsModule,FlexLayoutModule,MainHeaderComponent,SearchSmthComponent,MaterialModule,CountriesAdvertisementComponent,NgxCarouselModule,NgxDaterangepickerMd],
    entryComponents: [ExampleHeader],
    providers: [
        {
          provide: SWIPER_CONFIG,
          useValue: DEFAULT_SWIPER_CONFIG
        },
        LocaleService
      ]
})
export class SharedModule { }