import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CitiesService } from './cities.service';
const cities = [
    {
        image: '/assets/assets/images/yerevan.jpg',
        nameOfCity: "Yerevan",
        text: "Yerevan is very beautiful city"
    },
    {
        image: '/assets/assets/images/gyumri.jpg',
        nameOfCity: "Gyumri",
        text: "Gyumri is very beautiful city"
    },
    {
        image: '/assets/assets/images/kirovakan.jpg',
        nameOfCity: "Kirovakan",
        text: "Kirovakan is very beautiful city"
    },
    {
        image: '/assets/assets/images/artik.jpg',
        nameOfCity: "Artik",
        text: "Artik is very beautiful city"
    },
    {
        image: '/assets/assets/images/yerevan.jpg',
        nameOfCity: "Yerevan",
        text: "Yerevan is very beautiful city"
    },
    {
        image: '/assets/assets/images/gyumri.jpg',
        nameOfCity: "Gyumri",
        text: "Gyumri is very beautiful city"
    },
    {
        image: '/assets/assets/images/kirovakan.jpg',
        nameOfCity: "Kirovakan",
        text: "Kirovakan is very beautiful city"
    },
    {
        image: '/assets/assets/images/artik.jpg',
        nameOfCity: "Artik",
        text: "Artik is very beautiful city"
    }


]
@Component({
    selector: 'cities',
    templateUrl: './cities.component.html',
    styleUrls: ['./cities.component.scss']
})
export class CitiesComponent implements OnInit {
    public cities = cities;
    public countryCode: string;
    public citiesList;
    constructor(private _activatedRoute: ActivatedRoute, private _citiesService: CitiesService,private _router: Router) {
        this._activatedRoute.queryParams
            .subscribe(params => {
                if (params) {
                    console.log(params.region);
                    this.countryCode = params.region
                }
            });
    }
    ngOnInit() {
        this.getCitiesOfCountry()
    }

    public getCitiesOfCountry() {
        this._citiesService.getCityWithCountryCode(this.countryCode).subscribe((data) => {
            console.log(data, "cities");
            this.citiesList = data;
            

        })
    }
    public goToPageOfCity(id){
        this._router.navigate(['/cities',id],{relativeTo: this._activatedRoute,queryParams: { cityId:id } })
    
    }
 

}