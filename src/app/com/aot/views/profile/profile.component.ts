import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { ProfileService } from './profile.service';
import { ClientDatas } from '../../models/models';
const menuFields = [
    {
        name: "INFO",
        routerLink: "userInfo"
    },
    {
        name: "SETTINGS",
        routerLink: "settings"
    },
    {
        name: "HISTORY OF SEARCH",
        routerLink: "historyOfSearch"
    },
    {
        name: "REVIEW",
        routerLink: "review"
    }

]
@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    public menuFields = menuFields;
    public clientInfo: ClientDatas;
    constructor(private _router: Router, private _cookieService: CookieService, private _profileService: ProfileService) { }
    ngOnInit() {
        this.getMyData()
    }
    public clickToLogOut() {
        this._cookieService.remove("token")
        this._router.navigate(['/login'])

    }

    public getMyData(): void {
        this._profileService.getMyData().subscribe((data:ClientDatas) => {
            console.log(data, "my data");
            this.clientInfo = data
        })
    }



}