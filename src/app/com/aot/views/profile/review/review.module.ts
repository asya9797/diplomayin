import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ReviewService } from "./review.service";
import { ReviewRoutingModule } from "./review-routing.module";
import { ReviewComponent } from "./review.component";
import { SharedModule } from "../../../shared/shared.module";



@NgModule({
    declarations:[ReviewComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,ReviewRoutingModule,SharedModule],
    exports:[],
    providers:[ReviewService]
})
export class ReviewModule {

}