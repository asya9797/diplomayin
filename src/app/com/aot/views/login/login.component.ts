import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { CookieService } from 'angular2-cookie/core';
import { Router } from '@angular/router';
import { AccessToken, LoginFields } from '../../models/models';
@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public loginForm: FormGroup
    constructor(private _loginService:LoginService,private _cookieService: CookieService,private _router: Router) { }
    ngOnInit() { 
        this._buildForm()
       
    }


    private _buildForm() {
        this.loginForm = new FormBuilder().group({
            email: ['', Validators.required],
            password: ['', Validators.required],
        })
    }
    public clickEnter(){
        console.log( this.loginForm.value.email);
        console.log( this.loginForm.value.password);
    }

    public clickToLogin(){
        let sendingData:LoginFields = {
            email: this.loginForm.value.email,
            password: this.loginForm.value.password
        }
        this._loginService.login(sendingData).subscribe((data:AccessToken) => {
            this._cookieService.put("token", data.accessToken);
            this._router.navigate(["/profile"])
            console.log(data.accessToken,"dataa loginn");
            
        })
    }
}