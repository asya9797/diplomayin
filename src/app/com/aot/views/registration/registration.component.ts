import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from './registration.service';
import { CountriesList } from '../../models/models';
@Component({
    selector: 'registration',
    templateUrl: './registration.component.html',
    styleUrls: ['registration.component.scss']
})
export class RegistrationComponent implements OnInit {
    public registrationForm: FormGroup;
    public countries:Array<CountriesList>;
    public matchingPasswordsError: string = undefined;
    private _emailPattern: string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    constructor(private router: Router, private _registrationService: RegistrationService,private _router: Router) { }
    ngOnInit() {
        this._buildForm();
        this.getCountriesList();
        console.log(this.registrationForm.get("country").valueChanges.subscribe((data) =>{
        console.log(data);
        }));
        console.log(this.registrationForm.get("gender").valueChanges.subscribe((data) =>{
            console.log(data);
            }));
    }

    private _buildForm() {
        this.registrationForm = new FormBuilder().group({
            username: ['', Validators.required],
            email: ['', [Validators.required, Validators.pattern(this._emailPattern)]],
            address: ['', Validators.required],
            phoneNum: ['', Validators.required],
            age: ['', Validators.required],
            gender: ['',Validators.required],
            city: ['', Validators.required],
            country: ['', Validators.required],
            zipCode: ['', Validators.required],
            password: ['', Validators.required],
            repeatPassword: ['', Validators.required]

        },{ validator: this.matchingPasswords('password', 'repeatPassword') })
        this.registrationForm.valueChanges.subscribe((data) => {
            if (this.registrationForm.value.password != this.registrationForm.value.repeatPassword && this.registrationForm.get("password").dirty && this.registrationForm.get("repeatPassword").dirty) {
                this.matchingPasswordsError = "Passwords don't match"
            } else {
                this.matchingPasswordsError = undefined
            }
        })
        
        
    }
    private matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];
            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        }
    }

    public navigateToMainPage() {
        this.router.navigate([''])
    }
    public handleAddressChange(address) {
        console.log(address);

    }

    public getCountriesList() {
        this._registrationService.getCountriesList().subscribe((data:Array<CountriesList>) => {
            console.log(data);
            this.countries = data

        })
    }

    public postClientDatasForRegistration(){
        let sendingData = {
            userName: this.registrationForm.value.username,
            email: this.registrationForm.value.email,
            address: this.registrationForm.value.address,
            phone: this.registrationForm.value.phoneNum,
            age: +this.registrationForm.value.age,
            gender: this.registrationForm.value.gender,
            city: this.registrationForm.value.city,
            country: this.registrationForm.value.country,
            // zipCode:this.registrationForm.value.zipCode,
            password: this.registrationForm.value.password
        }
        this._registrationService.postClientDatasForRegistration(sendingData).subscribe((data) => {
            console.log("registration",data);
            this._router.navigate(['/login'])
            
        })
    }
}