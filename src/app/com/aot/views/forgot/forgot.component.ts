import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
    selector: 'forgot',
    templateUrl: './forgot.component.html',
    styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {
    public forgotForm: FormGroup;
    constructor() { }
    ngOnInit() {
        this._buildForm()
     }


    private _buildForm() {
        this.forgotForm = new FormBuilder().group({
            email: ['', Validators.required],
            

        })
    }
}