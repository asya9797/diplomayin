import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'angular2-cookie/core';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of } from 'rxjs';


@Injectable()
export class ApiService {
   

    constructor(@Inject('BASE_URL') private baseUrl,
        private _httpClient: HttpClient,
        private _cookieService: CookieService,
        private _router: Router,
    ) { }


    public get(url: string, observe?, responseType?) {
        let accessToken = this._cookieService.get('token') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.get(this.baseUrl + url, params)
    }


    public post(url: string, body: object, observe?, responseType?) {
        let accessToken = this._cookieService.get('token') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'token': accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.post(this.baseUrl + url, body, params);
    }

    public postFormData(url: string, formData: FormData, observe?, responseType?) {
        let accessToken = this._cookieService.get('token') || '';
        let headers = new HttpHeaders({
            'token': accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.post(this.baseUrl + url, formData, params);
    }

    
    public put(url: string, body: object, observe?, responseType?) {
        let accessToken = this._cookieService.get('token') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'token': accessToken,
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.put(this.baseUrl + url, body, params);
    }
   
    public delete(url: string, observe?, responseType?) {
        let accessToken = this._cookieService.get('token') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'token': accessToken
        })
        let params = { headers: headers };
        if (observe == 'response')
            params['observe'] = 'response';
        if (responseType == 'text')
            params['responseType'] = 'text';

        return this._httpClient.delete(this.baseUrl + url, params);
    }

    public checkAccessToken() {
        let accessToken = this._cookieService.get('token') || '';
        let headers = new HttpHeaders({
            'Content-type': 'application/json',
            'Authorization': accessToken,
         
        })
        return this._httpClient.get(this.baseUrl + 'client/checktoken', { headers: headers })
            .pipe(
                map((data) => {
                    return true;
                }),
                catchError((err, caught) => {
                    return of(false) ;
                }))
    }


    private _updateCookies(data) {
        this._cookieService.put('token', data.message.token);
    }
}