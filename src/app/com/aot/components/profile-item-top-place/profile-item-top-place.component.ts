import {Component,OnInit, Input} from '@angular/core';

@Component({
    selector: "profile-item-top-place",
    templateUrl: "./profile-item-top-place.component.html",
    styleUrls : ["./profile-item-top-place.component.scss"]

})

export class ProfileItemTopPlaceComponent implements OnInit{
    @Input() title:string;
    constructor(){}
    ngOnInit(){}
}