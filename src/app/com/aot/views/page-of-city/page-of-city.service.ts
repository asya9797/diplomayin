import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ServerRequests } from '../../services/serverRequests.service';


@Injectable()
export class PageOfCityService {
    constructor(private _serverRequestsService:ServerRequests) { }
    
    public getCityById(cityId:number):Observable<object | any>{
        return this._serverRequestsService.get(`city/${cityId}`)
    }
    public getPlacesOfCity(cityId:number):Observable<object | any>{
        return this._serverRequestsService.get(`place/bycityid/${cityId}`)
    }
}