import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RegistrationService } from "./registration.service";
import { RegistrationRoutingModule } from "./registration-routing.module";
import { RegistrationComponent } from "./registration.component";
import { SharedModule } from "../../shared/shared.module";
import { ServerRequests } from "../../services/serverRequests.service";


@NgModule({
    declarations:[RegistrationComponent,],
    imports:[CommonModule,RegistrationRoutingModule,FormsModule,ReactiveFormsModule,SharedModule],
    exports:[],
    providers:[RegistrationService,ServerRequests],
    entryComponents: []
})
export class RegistrationModule {

}