import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { PageOfCityComponent } from "./page-of-city.component";
const routesPageOfCity: Routes = [
    {
        path:"",
        component: PageOfCityComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesPageOfCity)],
    exports:[RouterModule]
})
export class PageOfCityRoutingModule{}