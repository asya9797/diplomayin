import { Component, OnInit } from '@angular/core';
import { StatisticsService } from './statistics.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'statistics',
    templateUrl: './statistics.component.html',
    styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {
    public origin;
    public destination;
    public day;
    constructor(private _statisticsService: StatisticsService, private _activatedRoute: ActivatedRoute) {
        this._activatedRoute.queryParams
            .subscribe(params => {
                if (params) {

                    this.origin = params.origin;
                    this.destination = params.destination;
                    this.day = params.day
                }
            });
    }
    ngOnInit() { 
        this.getTickets()
    }

    public getTickets() {
        this._statisticsService.getTickets(this.origin,this.destination,this.day).subscribe((data) => {
            console.log(data, "tickets");

        })
    }

}


