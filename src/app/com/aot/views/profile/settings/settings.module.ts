import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { SettingsService } from "./settings.service";
import { SettingsRoutingModule } from "./settings-routing.module";
import { SettingsComponent } from "./settings.component";
import { SharedModule } from "../../../shared/shared.module";



@NgModule({
    declarations:[SettingsComponent],
    imports:[CommonModule,FormsModule,ReactiveFormsModule,SettingsRoutingModule,SharedModule],
    exports:[],
    providers:[SettingsService]
})
export class SettingsModule {

}