import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { SearchHistoryComponent } from "./search-history.component";
const routesSearchHistory: Routes = [
    {
        path:"",
        component: SearchHistoryComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesSearchHistory)],
    exports:[RouterModule]
})
export class SearchHistoryRoutingModule{}