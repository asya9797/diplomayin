import { Injectable } from '@angular/core';
import { ServerRequests } from '../../services/serverRequests.service';
import { Observable } from 'rxjs';

@Injectable()
export class CitiesService {
    constructor(private _serverRequestsService: ServerRequests) { }

    public getCityWithCountryCode(code:string):Observable<object | any>{
        return this._serverRequestsService.get(`city/bycountrycode/${code}`)
    }
    
}