import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './com/aot/services/authguard.service';
import { ApiService } from './com/aot/services/api.service';
import { CookieService } from '../../node_modules/angular2-cookie/core';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: 'BASE_URL',useValue: 'http://192.168.0.141:8080/'
     },
     AuthGuard,
     ApiService,
     CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
